<?
	require_once("inc/beg.php");

	class testrepeats extends Page {
		function construct() {
			$this->list1->data = range(1, 10);
			foreach($this->list1 as $k=>$v) {
				$v->list2->data = range(11, 20);
				foreach($v->list2 as $kk=>$vv) {
					$vv->label1->caption = "location $k, $kk";
				}
			}
		}

		function button1_click($a, $b) {
			print "a is $a, b is $b";
		}
	}

	$a = new testrepeats;
	$a->render();
?>
