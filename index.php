<?
	ini_set("zend.ze1_compatibility_mode", false);
	$starttime = microtime(true);
	$page = "todo";
	require_once("login.php");
	require_once("$page.php");
	$auth = new Login;
	if($auth->authenticated() == false) {
		$auth->render();
		exit();
	} else {
		$auth->fire_events(); // click logout
	}
	$page = new $page;
	$page->render();

	$elapsed = microtime(true) - $starttime;
	print "$elapsed seconds, " . memory_get_usage() . " bytes";
?>
