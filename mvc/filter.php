<?
	class Filter implements IteratorAggregate {
		protected $filters = array();
		public $has_filters = false;
		public function add($var, $comp, $val) {
			$r = array();
			$r['var'] = $var;
			$r['comp'] = $comp;
			$r['val'] = $val;
			$this->filters[] = $r;
			$this->has_filters = true;
		}
		public function matches($object) {
			foreach($this->filters as $filter) {
				$val = $this->getval($object, $filter['var']);
				if(!$this->comp($val, $filter['comp'], $filter['val'])) {
					return false;
				}
			}
			return true;
		}

		protected function getval($object, $var) {
			if(is_array($object)) return $object[$var];
			if(is_object($object)) return $object->$var;
			die("couldn't get $var from object");
		}

		protected function comp($val1, $comp, $val2) {
			switch($comp) {
				case '==': return $val1 == $val2; break;
				case '!=': return $val1 != $val2; break;
				// add comparisons as needed
				default: die("invalid comp $comp'"); break;
			}
		}

		function getIterator() {
			return new ArrayIterator($this->filters);
		}
	}

	class Sort implements IteratorAggregate {
		protected $sorts = array();
		public $has_sorts = false;
		function add($item, $direction = "asc") {
			$r['item'] = $item;
			$r['direction'] = $direction;
			$this->sorts[] = $r;
			$this->has_sorts = true;
		}

		function getIterator() {
			return new ArrayIterator($this->sorts);
		}
	}

?>
