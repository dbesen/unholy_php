<?
	class Lister extends PageElement implements ArrayAccess, IteratorAggregate { // gimpy that we can't just call it List
		public $items = array(); // todo: make this protected again, only public for print_tree
		function set_data($ds) {
			foreach($ds as $k=>$v) {
				$this->add_item($k, $v);
			}
		}

		function add_item($key, $row = null) {
			$a = new PageElement;
			$a->parent = $this;
			$a->key = $key;
			foreach($this->children as $child) {
				$n = clone($child);
				if($row !== null) {
					if($n->type == "label") {
						if(is_object($row)) $data = $row->{$n->name};
						else $data = $row[$n->name];
						$n->caption = htmlentities($data);

					}
				}
				$n->parent = $a;
				$a->children[] = $n;
			}
			$a->user_row_data = $row;
			$this->items[$key] = $a;
		}

		function render() {
			foreach($this->items as $k=>$item) {
				if(!$item->visible) continue;
				if(method_exists($this, "lister_render")) $this->lister_render($k, $item->user_row_data);
				$item->render();
			}
		}

		function get_first() {
			$keys = array_keys($this->items);
			$first = array_shift($keys);
			return $this->items[$first];
		}

		function get_last() {
			$keys = array_keys($this->items);
			$last = array_pop($keys);
			return $this->items[$last];
		}

		function get_count() {
			return count($this->items);
		}

		function getIterator() {
			return new ArrayIterator($this->items);
		}

		function offsetExists($o) { return true; }
		function offsetGet($offset) {
			if(!isset($this->items[$offset])) $this->add_item($offset);
			return $this->items[$offset];
		}
		function offsetSet($offset, $val) { trigger_error("not implemented"); }
		function offsetUnset($offset) { trigger_error("not implemented"); }
	}

	class Button extends PageElement {
		public $caption = null; // so it can be overridden by page
		function render() {
			if($this->caption != null) $caption = $this->caption;
			else if(isset($this->attributes['caption'])) $caption = $this->attributes['caption'];
			else $caption = $this->name;
			if(isset($this->attributes['default'])) {
				print "<input type=\"hidden\" name=\"default\" value=\"{$this->full_name()}\" />";
			}
			$this->remove_attr("caption");
			$this->remove_attr("name");
			$this->remove_attr("default");
			$n = "click[{$this->full_name()}]"; // urlencode?
			print "<input type=\"submit\" name=\"$n\" value=\"{$caption}\"{$this->astr} />";
		}
	}

	class Link extends PageElement {
		public $disabled = false;
		public $caption = null;
		function predraw() {
			if(isset($this->attributes['href'])) {
				$href = $this->attributes['href'];
				unset($this->attributes['href']);
			}
			else $href = $this->get_href();
			if($this->caption != null) $caption = $this->caption;
			elseif(isset($this->attributes['caption'])) $caption = $this->attributes['caption'];
			else $caption = "";
			$this->remove_attr("caption");
			$this->remove_attr("name");
			$this->remove_attr("type");
			$this->remove_attr("val");
			if($this->disabled) {
				print $caption;
			} else {
				print "<a href=\"$href\"{$this->astr}>{$caption}";
			}
		}
		function postdraw() {
			print "</a>";
		}

		protected function get_href() {
			$this->parse_params(); // normally only the page cares what params are
			if(!isset($this->attributes['type'])) return $this->build_url(urlencode("click[{$this->full_name()}]"), "");

			if($this->attributes['type'] == 'param') { // should these be called "state" links? or something?
				$key = $this->single_key; // should this support multiple keys?
				if($key == "") $key = $this->attributes['val'];
				if($key === null) {
					trigger_error("parameter link without val");
				}
				return $this->build_url($this->attributes['name'], $key);
			}

			if($this->attributes['type'] == 'toggle') {
				if(isset($this->params[$this->attributes['name']]))
					return $this->build_url($this->attributes['name'], null);
				return $this->build_url($this->attributes['name'], "");
			}

			trigger_error("invalid link type {$this->attributes['type']}");
		}
	}

	class Textbox extends PageElement {
		protected $mytext = "";
		function get_text() {
			$asdf = $this->get_request($this->full_name());
			if($asdf !== null) $this->mytext = $asdf;
			return $this->mytext;
		}
		function set_text($val) {
			$this->mytext = $val;
		}
		function render() {
			$type = "text";
			if(isset($this->attributes['type'])) $type = $this->attributes['type'];
			$this->remove_attr("type");
			print "<input type=\"$type\" value=\"{$this->text}\"{$this->astr} />";
		}
	}

	class Label extends PageElement {
		public $caption = "";
		function render() {
			parent::render();
			print $this->caption;
		}
	}

	class SelectBox extends PageElement {
		public $data = array();
		protected $curr_selected = null;

		function get_selected() {
			$asdf = $this->get_request($this->full_name());
			if($asdf !== null) $this->curr_selected = $asdf;
			return $this->curr_selected;
		}

		function set_selected($selected) {
			$this->curr_selected = $selected;
		}

		function render() {
			print "<select {$this->astr}>";
			foreach($this->data as $k=>$v) {
				$c = "";
				if($this->selected == $k) {
					$c = " selected=\"selected\"";
				}
				print "<option value=\"$k\"$c>$v</option>";
			}
			print "</select>";
		}
	}

	class Checkbox extends PageElement {
		protected $is_checked = false;
		function get_checked() {
			$asdf = $this->get_request($this->full_name());
			$this->is_checked = ($asdf !== null);
			return $this->is_checked;
		}
		function set_checked($val) {
			$this->is_checked = (boolean) $val;
		}
		function render() {
			$c = "";
			if($this->is_checked) {
				$c = " checked=\"checked\"";
			}
			print "<input type=\"checkbox\"$c{$this->astr} />";
		}
	}

	// todo: checkboxgroup, but how to pass data to page?
	// todo: radiobutton, but where to get name?

	class RadioButtonGroup extends lister {
		protected $curr_selected = null;

		function get_selected() {
			$asdf = $this->get_request($this->full_name());
			if($asdf !== null) $this->curr_selected = $asdf;
			return $this->curr_selected;
		}

		function set_selected($selected) {
			$this->curr_selected = $selected;
		}

		function set_checked($a) { // cause "checked" vs. "selected" is hard to remember
			$this->set_selected($a);
		}
		function get_checked() {
			return $this->get_selected();
		}

		function lister_render($k, $v) {
			$c = "";
			if($this->selected == $k) {
				$c = " checked=\"checked\"";
			}
			print "<input value=\"$k\" type=\"radio\"$c{$this->astr} />$v";
		}
	}

?>
