<?
	function stripslashes_deep($value) {
		$value = is_array($value) ?  array_map('stripslashes_deep', $value) : stripslashes($value);
		return $value;
	}

	function _print_r($val) {
		print "<pre>";
		print_r($val);
		print "</pre>";
	}

	require_once("error.php");
	require_once("db.php");
	require_once("pages.php");
?>
