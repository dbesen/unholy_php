<?
	function print_tree($item, $level = 0) {
		if($item instanceOf TextElement) {
			return;
		}
		print str_repeat(" ", $level * 10);
		print $item->type . " " . $item->name . "(<font color=red>" . $item->key . "</font>) " . get_class($item) . " {";
		$children = $item->children;
		if(isset($item->items)) $children = $item->items;
		if($children) {
			print "\n";
			foreach($children as $v) {
				print_tree($v, $level+1);
			}
			print str_repeat(" ", $level * 10);
		}
		print "}\n";
	}

	class PageElement {
		public $children = array();
		public $name = "";
		protected $attributes = array();
		protected $removed_attrs = array();
		public $type = "";
		public $visible = true;
		protected $mykey = null; // for lister
		public $parent = null; // for lister
		protected $params = array(); // query string variables
		public $user_row_data = null; // for lister/radiobuttongroup

		final function __set($var, $val) {
			$fname = "set_" . $var;
			return $this->$fname($val);
		}

		final function __get($var) {
			// check if we know how to get this
			$fname = "get_" . $var;
			if(method_exists($this, $fname)) return $this->$fname();

			foreach($this->children as $child) {
				if($child->name == $var) return $child;
			}

			trigger_error("couldn't find var $var");
		}

		function __clone() { // ouch, expensive.. for lister
			foreach($this->children as $k=>$child) {
				$this->children[$k] = clone($child);
				$this->children[$k]->parent = $this; // WTF? why doesn't clone copy this reference?
			}
		}

		function parse_params() {
			$this->params = $_GET;
			unset($this->params['click']);
		}

		function get_request($varname) {
			if(!isset($_REQUEST[$varname])) return null;
			$asdf = $_REQUEST[$varname];
			if(get_magic_quotes_gpc()) $asdf = stripslashes_deep($asdf);
			return $asdf;
		}

		protected function full_name() {
			$a = $this->get_key();
			if(strlen($a)) $a .= "/";
		       	$a .= $this->name;
			return $a;
		}

		protected function get_key() {
			$key = "";
			if($this->parent !== null) {
				$key .= $this->parent->get_key();
			}
			if($this->mykey !== null) $key .= "K" . $this->mykey;
			return $key;
		}

		protected function get_single_key() {
			if($this->mykey !== null) return $this->mykey;
			if($this->parent !== null) {
				return $this->parent->get_single_key();
			}
			die("couldn't find key");
		}

		protected function set_key($val) {
			$this->mykey = $val;
		}

		protected function load_template() {
			$dom = new DomDocument();
			$dom->substituteEntities = false;
			$file = file_get_contents($this->template_name());
			// todo: replace all unrecognized tags with &gt stuff
			$file = $this->preparse($file);
			$dom->loadXML($file);
			$this->get_from_dom($dom);
		}

		protected function get_valid_classes() { // todo: this is gimpy
			$file = file("mvc/pageelements.php");
			$lines = preg_grep("/^\s+class/i", $file);

			$ret = array();
			foreach($lines as $line) {
				if(!preg_match("/^\s+class (\S+)/", $line, $matches)) continue;
				$classname = strtolower($matches[1]);
				if(class_exists($classname)) $ret[$classname] = true;
			}

			unset($ret['lister']);
			$ret['list'] = true;

			return array_keys($ret);

		}

		protected function preparse($file) {
			$file = str_replace("&", "&amp;", $file);
			foreach($this->get_valid_classes() as $classname) {
				$file = preg_replace("/<([^>]*{$classname}[^>]*)>/i", '&stuff;$1&endstuff;', $file);
			}
			$file = str_replace("<", "&lt;", $file);
			$file = str_replace(">", "&gt;", $file);
			$file = str_replace("&stuff;", "<", $file);
			$file = str_replace("&endstuff;", ">", $file);
			return "<stuff>" . $file . "</stuff>";
		}

		protected function template_name() {
			return "template/" . strtolower(get_class($this)) . ".xhtml";
		}

		protected function get_from_dom($dom) {
			if($dom->hasAttributes()) {
				foreach($dom->attributes as $v) {
					$this->set_attr($v->name, $v->value);
				}
			}
			if($dom->hasChildNodes()) {
				foreach($dom->childNodes as $n) {
					if($dom->nodeType == XML_DOCUMENT_NODE) {
						// ignore it
						foreach($dom->childNodes as $n) {
							$this->get_from_dom($n);
						}
						continue;
					}
					// create object
					$classname = strtolower($n->nodeName);
					if($classname == "#text") {
						$this->children[] = new TextElement($n->nodeValue);
						continue;
					}
					if($classname == "#comment") {
						$this->children[] = new TextElement("<!--" . str_replace("&amp;", "&", $n->nodeValue) . "-->");
						continue;
					}
					if($classname == "list") $classname = "lister";
					$new_object = new $classname();

					$new_object->parent = $this;
					$new_object->get_from_dom($n);
					$new_object->type = $n->nodeName;
					$this->children[] = $new_object;
				}
			}
		}

		protected function predraw() {
			if($this->type == "") return;
			if(get_class($this) != "PageElement") return; // so we don't get "<label.." in the source, for things that do parent::render
			if(count($this->children)) {
				print "<{$this->type}{$this->astr}>";
			} else {
				print "<{$this->type}{$this->astr} />";
			}
		}

		protected function postdraw() {
			if($this->type == "") return;
			if(count($this->children)) {
				print "</{$this->type}>";
			}
		}

		protected function call_func($fname, $args = null) { // return whether or not a function was called
			if(strstr($fname, ".")) {
				list($classname, $fname) = explode(".", $fname);
				if(strtolower(get_class($this)) !== strtolower($classname)) {
					return false;
				}
			}
			if(!method_exists($this, $fname)) return false;
			if($args == null) {
				$this->$fname();
			} else {
				call_user_func_array(array($this, $fname), $args);
			}
			return true;
		}

		public function fire_events() {
			// this function is only called for top-level (i.e. user) objects

			// first, param set events
			foreach($this->params as $k=>$v) {
				$fname = $k . "_set";
				if(method_exists($this, $fname)) $this->$fname($v);
			}

			// next, click events
			$clicked = false;
			$keys = array();
			$clicks = $this->get_request('click');
			if($clicks !== null) {
				foreach($clicks as $k=>$v) {
					if(strstr($k, "/")) {
						$items = explode("/", $k);
						$keys = explode("K", substr($items[0], 1)); // substr since first char is K
						$fname = $items[1] . "_click";
						if($this->call_func($fname, $keys)) $clicked = true;
					} else {
						$fname = $k . "_click";
						if($this->call_func($fname)) $clicked = true;
					}
				}
			} else {
				if(count($_POST)) { // form was submitted by enter being pressed, we have to find a default action
					// we have to pass the default name via the client since otherwise we can't have
					// a different action depending on which form was submitted
					if(isset($_POST['default'])) {
						$fname = $_POST['default'] . "_click";
						if($this->call_func($fname)) $clicked = true;
					}
				}
			}
			if($clicked) {
				$str = $this->build_url();
				header("Location: $str");
				exit;
			}
		}

		function build_url($var = null, $val = null) {
			$str = "";
			foreach($this->params as $k=>$v) {
				if($k == $var) continue;
				if($v === "") $str .= "&$k";
				else $str .= "&$k=$v";
			}

			if($var !== null && $val !== null) {
				if($val === "") $str .= "&$var";
				else $str .= "&$var=$val";
			}

			if($str) $str{0} = '?';
			return $GLOBALS['PHP_SELF'] . $str;
		}

		public function render() {
			$this->fire_events();
			if(!$this->visible) return;
			if(method_exists($this, "predraw")) $this->predraw();
			foreach($this->children as $child) {
				if(!$child->visible) continue;
				$child->render();
			}
			if(method_exists($this, "postdraw")) $this->postdraw();
		}

		protected function set_attr($name, $val) {
			if($name == "visible") {
				if($val == "true") $val = true;
				if($val == "false") $val = false;
				$this->visible = $val;
				return;
			}
			// we don't want to set name on default PageElement things because if you do
			// <form name="jkl"> we don't want it to affect the full names of everything
			// in the form
			if($name == "name" && (get_class($this) != "PageElement")) $this->name = $val;
			$this->attributes[$name] = $val;
		}

		protected function remove_attr($attrname) { // for astr
			$this->removed_attrs[strtolower($attrname)] = true;
		}

		protected function get_astr() {
			$astr = "";
			foreach($this->attributes as $k=>$v) {
				if(!isset($this->removed_attrs[strtolower($k)])) {
					if($k == "name") $v = $this->full_name();
					$astr .= " $k=\"$v\"";
				}
			}
			return $astr;
		}
	}

	class TextElement { // to increase speed.. believe it or not, this is faster than storing a string and calling is_object
		public $text = "";
		public $name = null;
		public $type = null;
		public $visible = true;
		public function __construct($text = "") {
			$this->text = $text;
		}
		function render() {
			print $this->text;
		}
	}

	class Page extends PageElement {
		final function __construct() { // todo: let children call their constructors "__construct"
			$this->parse_params();
			$this->load_template();
			if(method_exists($this, "construct")) $this->construct();
		}
		function predraw() {
			$this->nocache_headers();
		}
		function nocache_headers() {
			if(headers_sent()) {
				print "Couldn't send nocache headers<br>\n";
			} else {
				header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
				header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
				header("Cache-Control: no-store, no-cache, must-revalidate");
				header("Cache-Control: post-check=0, pre-check=0", false);
				header("Pragma: no-cache");
			}
		}

	}

	require_once("pageelements.php");
?>
