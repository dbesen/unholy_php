<?
	require_once("filter.php");
	class Database {
		function __get($var) { die("$var not found"); }
		public $dbhandle;
		final private function __construct() {
			// connection pool instead?
			$uname = trim(`uname -n`);
			$host = "";
			if($uname == "s1") $host = "host=calendar ";
			$this->dbhandle = pg_connect("{$host}dbname=landings user= password=");
		}
		static public function singleton() {
			static $instance = null;
			if($instance == null) {
				$instance = new Database();
			}
			return $instance;
		}
	}

	abstract class DBInteractor {
		public $lastquery = "";
		protected function query($q) {
			global $track_all_queries;
			global $queries;
			$this->lastquery = $q;
			if($track_all_queries) $queries[] = $q;
			return pg_query(Database::singleton()->dbhandle, $q);
		}
		protected function dbinsert($table, $data) {
			$this->lastquery = "explicit pg_insert $table: " . serialize($data);
			$ret = pg_insert(Database::singleton()->dbhandle, $table, $data);
			if($ret == false) die("Insert failed");
		}
		protected function make_where($filter) {
			if(!$filter->has_filters) return "";
			$f = array();
			foreach($filter as $filter) {
				$var = pg_escape_string($filter['var']);
				$comp = $filter['comp'];
				if($comp == "==") $comp = "=";
				$val = $filter['val'];
				if($val === false) $val = 'f';
				if($val === true) $val = 't';
				$val = pg_escape_string($val);
				$f[] = $var . " " . $comp . " '" . $val . "'";
			}
			return " where " . implode(" and ", $f);
		}
		protected function make_sort($sort) {
			if(!$sort->has_sorts) return "";
			$s = array();
			foreach($sort as $sort) {
				$item = $sort['item'];
				$direction = $sort['direction'];
				$s[] = $item . " " . $direction;
			}

			return " order by " . implode(", ", $s);
		}
	}

	class DBTable extends DBInteractor implements Iterator, ArrayAccess {
		protected $keyon = "id";
		public $columns = null;
		protected $tablename;
		public $filter;
		public $sort;
		public $schema = null;
		final public function __construct($tablename = null, $var = null, $comp = null, $val = null) {
			// the last 3 vars are an optional shortcut for creating 1 filter.. maybe should be an array
			if($tablename == null) $tablename = get_class($this);
			$this->tablename = $tablename;
			$this->filter = new filter();
			$this->sort = new sort();
			if($var !== null) {
				$this->add_filter($var, $comp, $val);
			}
		}

		public function keyon($keyon) {
			$this->keyon = $keyon;
		}
		public function add_filter() { // shortcut
			$args = func_get_args();
			call_user_func_array(array($this->filter, "add"), $args);
		}

		public function add_sort() {
			$args = func_get_args();
			call_user_func_array(array($this->sort, "add"), $args);
		}
		public function clear_filter() {
			$this->filter = new filter();
		}

		protected function add_column($column) { // public?
			if($this->columns == null) return;
			if(!is_array($this->columns)) $this->columns = array($this->columns);
			if(array_search($column, $this->columns) === false) {
				$this->columns[] = $column;
			}
		}

		public function insert($data) {
			$this->dbinsert($this->tablename, $data);
		}

		// todo: join.. hardest part is figuring out the interface to it

		protected function make_query($filter = null) {
			if($filter == null) $filter = $this->filter;

			$this->add_column($this->keyon);

			$out = "select ";
			if($this->columns == null) $out .= "*";
			else $out .= implode(",", $this->columns);
			// todo: join tablenames
			$name = $this->tablename;
			if($this->schema != null) $name = $this->schema . "." . $name;
			$out .= " from $name";
			$out .= $this->make_where($filter);
			$out .= $this->make_sort($this->sort);
			return $out;
		}

		protected function make_dbrow($a, $b, $c, $d) {
			$classname = $this->tablename . "_dbrow";
			if(class_exists($classname)) {
				return new $classname($a, $b, $c, $d);
			}
			else return new DBRow($a, $b, $c, $d);
		}

		function get_first_row() {
			$this->rewind();
			if(!$this->valid()) return null;
			return $this->current();
		}

		protected $curr_key = null;
		protected $curr_result;
		protected $curr_result_resource = null;
		function current() {
			return $this->make_dbrow($this->curr_result, $this->tablename, $this->keyon, $this->filter);
//			return new DBRow($this->curr_result, $this->tablename, $this->keyon, $this->filter);
		}
		function key() {
			return $this->curr_key;
		}
		function valid() {
			if($this->curr_key === null) return false;
			return true;
		}
		function next() {
			$this->get_next();
		}
		function rewind() {
			if($this->curr_result_resource != null) pg_free_result($this->curr_result_resource);
			$this->curr_result_resource = $this->query($this->make_query());
			$this->get_next();
		}
		protected function get_next() {
			$data = pg_fetch_assoc($this->curr_result_resource);
			if($data === false) {
				$this->curr_key = null;
				return;
			}
			// if data[keyon] doesn't exist, error?
			$this->curr_key = $data[$this->keyon];
			$this->curr_result = $data;
		}

		protected $offsetcache = array();
		function offsetGet($offset) {
			if(!isset($this->offsetcache[$offset])) {
				$new_filter = clone $this->filter;
				$new_filter->add($this->keyon, "==", $offset);
				$res = $this->query($this->make_query($new_filter));
				$d = pg_fetch_assoc($res);
				if($d === false) {
					return null;
				}
				if(pg_fetch_assoc($res) !== false) {
					die("offsetGet got more than one row");
				}
				$this->offsetcache[$offset] = $this->make_dbrow($d, $this->tablename, $this->keyon, $new_filter);
			}
			return $this->offsetcache[$offset];
		}

		function offsetExists($offset) {
			if($this->offsetGet($offset) === null) return false;
			return true;
		}
		function offsetSet($offset, $val) {
			if($offset !== null) trigger_error("not implemented");
			$this->insert($val);
		}
		function offsetUnset($offset) {
			$asdf = $this->offsetGet($offset);
			$asdf->delete();
		}
	}

	class DBRow extends DBInteractor implements ArrayAccess { // iterator?
		public $changed = false;
		protected $data;
		protected $tablename;
		protected $keyon;
//		protected $filter; // we don't save this since changing it would probably break the __set stuff
		protected $where;
		function __construct($data, $tablename, $keyon, $filter) { // should only be called by DBTable
			$this->data = $data;
			$this->tablename = $tablename;
			$this->keyon = $keyon;
//			$this->filter = $filter;
			$filter = clone $filter;
			$filter->add($keyon, "==", $data[$keyon]); // important
			$this->where = $this->make_where($filter);
			if(method_exists($this, "construct")) $this->construct();
		}
		function __set($var, $val) {
			if($val === true) $val = 't';
			if($val === false) $val = 'f';
			if($this->data[$var] == $val) return;
			if($var == $this->keyon) {
				die("Cannot change keyon value: $var");
			}
			$this->changed = true;
			$this->data[$var] = $val;

			$var = pg_escape_string($var);
			$val = pg_escape_string($val);

//			$key = $this->data[$this->keyon];
			$this->query("update {$this->tablename} set $var = '$val'{$this->where}");
			// if it affected more than 1 row, rollback? or limit 1?
		}

		public function delete() {
			$this->query("delete from {$this->tablename}{$this->where}");
			// if it affected more than 1 row, rollback? or limit 1?
		}

		function __get($var) {
			if(!isset($this->data[$var])) {
				$fname = "get_" . $var;
				return $this->$fname();
			}
			$a = $this->data[$var];
			if($a == 'f') $a = false;
			if($a == 't') $a = true;
			return $a;
		}

		function offsetGet($offset) {
			return $this->__get($offset);
		}

		function offsetExists($offset) {
			if(isset($this->data[$offset])) return true;
			return false;
		}
		function offsetSet($offset, $val) {
			$this->__set($offset, $val);
		}
		function offsetUnset($offset) {
			trigger_error("not implemented");
		}
	}

	class projects_dbrow extends DBRow {
		protected $login;
		function construct() {
			$this->login = new DBTable("login");
			$this->login->columns = array("login");
		}
		function get_manager_namea() {
			$id = $this->data['manager'];
			return $this->login[$id]['login'];
		}
	}

?>
