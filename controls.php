<?
	require_once("inc/beg.php");
	class controls extends page {
		function construct() {
			$this->select1->data = range(1, 10);

			$this->check1->checked = true;

			$this->radio1->data = range(1, 10);
		}

		function go_click() {
			print "radio1 is " . (int) $this->radio1->checked . "<br>\n";
			print "check1 is " . (int) $this->check1->checked . "<br>\n";
			print "select1 is " . $this->select1->selected;
		}
	}

	$a = new controls;
	$a->render();
?>
