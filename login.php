<?
	require_once("mvc/beg.php");

	class Login extends Page {
		public $user;
		protected $pass;
		function construct() {
			if(isset($_COOKIE['login'])) {
				$this->user = $_COOKIE['login'];
				$this->pass = $_COOKIE['password'];
			}
		}

		function login_click() {
			$this->user = $this->login->text;
			$this->pass = md5($this->password->text);
			if($this->authenticated()) {
				setcookie("login", $this->user, null, "/");
				setcookie("password", $this->pass, null, "/");
			}
		}

		function authenticated() {
			$login = new DBTable("login");
			$login->keyon("login");
			$login->columns = array("login", "md5password");
			return ($login[$this->user]['md5password'] == $this->pass);
		}

		function logout_click() {
			setcookie("login", null, null, "/");
			setcookie("password", null, null, "/");
			header("Location: {$GLOBALS['PHP_SELF']}");
			exit();
		}
	}
?>
