<?
	require_once("mvc/beg.php");
	class Todo extends Page {
		private $todo;
		private $auth;
		function construct() {
			global $track_all_queries; // feature of db.php
			if(isset($this->params['showqueries'])) $track_all_queries = true;

			global $auth; // feature of login page
			$this->auth = $auth;
			$this->todo = new DBTable("todo_test", "username", "==", $this->auth->user);

			$done = new DBTable("todo_test", "username", "==", $this->auth->user);
			$done->columns = "item";
			$done->add_filter("done", "==", true);
			$done->add_sort("vieworder");

			$notdone = new DBTable("todo_test", "username", "==", $this->auth->user);
			$notdone->columns = "item";
			$notdone->add_filter("done", "==", false);
			$notdone->add_sort("vieworder");

			$this->list1->data = $notdone;
			$this->list2->data = $done;
			$this->username->caption = $this->auth->user;

			# Don't show links for items that can't be moved
			if($this->list1->count >= 1) {
				$this->list1->first->up->visible = false;
				$this->list1->last->down->visible = false;
			}
			if($this->list2->count >= 1) {
				$this->list2->first->up->visible = false;
				$this->list2->last->down->visible = false;
			}
		}

		function edit_set($val) {
			if(!is_numeric($this->params['edit'])) die("invalid edit id");
			$this->todo->columns = array("item", "done");
			if(!isset($this->todo[$this->params['edit']])) {
				die("invalid edit id");
			}
			$this->tb1->text = $this->todo[$this->params['edit']]['item'];
			$this->add->caption = "Submit";

			# don't show edit link for item being currently edited
			if($this->todo[$this->params['edit']]['done'] == true) {
				$this->list2[$this->params['edit']]->edit->disabled = true;
			} else {
				$this->list1[$this->params['edit']]->edit->disabled = true;
			}
		}

		function showqueries_set() {
			global $queries;
			$t = "";
			foreach($queries as $v) {
				$t .= "$v<br>\n";
			}
			$this->label_queries->caption = $t;
			$this->label_queries->visible = true;
			$this->showqueries->caption = "Don't show queries";
		}

		function up_click($key) {
			$this->move($key, "desc");
		}

		function down_click($key) {
			$this->move($key, "asc");
		}

		protected function move($key, $direction) { // direction is "asc" (down) or "desc" (up)
			# Retrieve pointers to relevant data
			$table = new DBTable("todo_test", "username", "==", $this->auth->user);
			$table->columns = array("done", "vieworder");
			$clicked_row = $table[$key];
			if($clicked_row->done) $table->add_filter("done", "==", true);
			else $table->add_filter("done", "==", false);

			# Calculate which element to swap with
			$table->add_sort("vieworder", $direction);

			$order = $clicked_row->vieworder;
			if($direction == "asc") $table->add_filter("vieworder", ">", $order);
			else $table->add_filter("vieworder", "<", $order);

			$swap_row = $table->get_first_row();
			if($swap_row === null) return;

			# Swap the orders
			$temp = $clicked_row->vieworder;
			$clicked_row->vieworder = $swap_row->vieworder;
			$swap_row->vieworder = $temp;
		}

		function delete_click($key) {
			unset($this->todo[$key]);
		}

		function add_click() {
			if(isset($this->params['edit'])) {
				$this->todo[$this->params['edit']]['item'] = $this->tb1->text;
				unset($this->params['edit']);
			} else {
				if(strlen($this->tb1->text) == 0) return;
				$r['item'] = $this->tb1->text;
				$r['username'] = $this->auth->user;
				$this->todo[] = $r;
			}
		}

		function done_click($key) {
			$this->todo[$key]->done = true;
		}

		function undone_click($key) {
			$this->todo[$key]->done = false;
		}

/*
		function __destruct() {
			print "<pre>";
			print_tree($this);
		}
*/
	}
?>
